function bt() {
    document.getElementById("inicioid").style.display = "none";
    document.getElementById("allpreguntas").style.display = "block";
    document.getElementById("genral").innerHTML = "<img src='imgs/576x768bb.png' alt=''>";
}

const preguntas = [{
        pregunta: "¿Cómo se llama la capital de Mongolia?<img width='100px' src='imgs/mongolia.jpg' alt=''>",
        respuesta: ["Ulan Bator", "Dakar", "Mirlanka", "Holuga"],

        correcta: 0
    },
    {
        pregunta: "¿De qué colores es la bandera de México? <img width='70px' src='imgs/mexico.jpeg' alt=''>",
        respuesta: [
            "Verde, blanco y rojo",
            "Amarillo, blanco y rojo",
            "Verde, negro y rojo",
            "Verde, azul y rojo"
        ],
        correcta: 0

    },

    {
        pregunta: "¿Cuándo acabó la II Guerra Mundial?  <img width='100px' src='imgs/guerra.jpg' alt=''>",
        respuesta: ["En 1945", "En 1946", "En 1944", "En 1943"],
        correcta: 0
    },
    {
        pregunta: "¿Cuál es el océano más grande?  <img width='100px' src='imgs/oceano.jpeg' alt=''> ",
        respuesta: ["Pacífico", "Atlántico", "Índico", "Ártico"],
        correcta: 0
    },
    {
        pregunta: "¿Qué producto cultiva más Guatemala?  <img width='100px' src='imgs/guatemala.jpg' alt=''>",
        respuesta: ["Café", "Arroz", "Azúcal", "Trigo"],
        correcta: 0
    },
    {
        pregunta: "¿Cuál es el país más grande del mundo?  <img width='100px' src='imgs/grande.jpg' alt=''>",
        respuesta: ["Rusia", "Austria", "USA", "China"],
        correcta: 0
    },
    {
        pregunta: "¿Cuál es el color que representa la esperanza?<img width='80px' src='imgs/esperanza.jpg' alt=''>",
        respuesta: ["Verde", "Azul", "Blanco", "Amarillo"],
        correcta: 0
    },
    {
        pregunta: "¿Quién ganó el mundial de 2014?  <img width='100px' src='imgs/futbol.jpg' alt=''>",
        respuesta: ["Brasil", "Francia", "España", "Alemania"],
        correcta: 0
    },
    {
        pregunta: "¿Cuántos corazones tienen los pulpos?  <img width='100px' src='imgs/pulpo.jpg' alt=''>",
        respuesta: ["5", "4", "3", "1"],
        correcta: 2
    },
    {
        pregunta: "¿Cuantos huesos tiene el cuerpo humano?<img width='80px' src='imgs/huesos.jpg' alt=''>",
        respuesta: ["Ulan Bator", "dakar", "mirlanka", "holuga"],
        correcta: 0
    }
];
// preguntas cine;
function cine() {
    document.getElementById("inicioid").style.display = "none";
    document.getElementById("allpreguntas").style.display = "block";
    document.getElementById("genral").innerHTML = "<img src='imgs/22.png' alt=''>";

}
const preguntasCine = [{
        pregunta2: "¿Qué personaje mata a la gente cuando sueñan?",
        respuesta2: ["Peppa Pig", "Freddy Krueger", "Joseba Montero", "Chucky", ],
        correcta2: 2
    },
    {
        pregunta2: "¿Cómo se llama la novia de Tyler Durden en El CLub De La Lucha?",
        respuesta2: [
            "Martha",
            "Matilda",
            "Maria",
            "Marla"
        ],
        correcta2: 2
    },

    {
        pregunta2: "¿Cuál es el nombre del actor que interpreta a Harry Potter?",
        respuesta2: ["Daniel Doughnut", "Diego Robles", "Daniel Radcliffe", "Daniel Craig"],
        correcta2: 2
    },
    {
        pregunta2: "Famosa película de los Monty Python: La vida de...",
        respuesta2: ["Brian", "Bryan", "Bruno", "Imanol"],
        correcta2: 2
    },
    {
        pregunta2: "¿Quién fue la primera princesa de Disney?",
        respuesta2: ["Cenicienta", "Yeray", "Blancanieves", "Aurora"],
        correcta2: 4
    },
    {
        pregunta2: "Dolor y Gloria es la última peli de...",
        respuesta2: ["Luis Enrique", "Luis Buñuel", "Iván Zulueta", "Pedro Almodovar"],
        correcta2: 2
    },
    {
        pregunta2: "¿De qué escuela es director Albus Dumbledore?",
        respuesta2: ["5Mentarios", "La escuela de Moha", "Peñascal", "Hogwharts"],
        correcta2: 2
    },
    {
        pregunta2: "¿Quién pilota el Halcón Milenario?",
        respuesta2: ["Cesar", "Solo", "Elbo", "Acompañado"],
        correcta2: 2
    },
    {
        pregunta2: "¿Cómo hicieron el sonido del Braquiosaurio en Parque Jurásico?",
        respuesta2: ["Trompeta", "Jose", "Cuerno", "Burro"],
        correcta2: 2
    },
    {
        pregunta2: "¿Cuál de estas películas se estrenó en 1960?",
        respuesta2: ["Psicosis", "El Show de Gontzal", "Buscando a Anto", "Taxi Driver"],
        correcta2: 2
    }
];


// CULTURA



randomItem = preguntas[Math.floor(Math.random() * preguntas.length)];


document.getElementById("pregunta").innerHTML = randomItem.pregunta;
document.getElementById("r1").innerText = randomItem.respuesta[0];
document.getElementById("r2").innerText = randomItem.respuesta[1];
document.getElementById("r3").innerText = randomItem.respuesta[2];
document.getElementById("r4").innerText = randomItem.respuesta[3];
//document.getElementById("image").innerText = '<img src="' + randomItem.image + '">';
var contador = 0;

var score = 0;

const btnext = document.getElementById("Next");
btnext.addEventListener("click", function() {

    if (document.querySelector('input[name="opciones"]:checked') === null) {
        alert("Elije una respuesta! Joputi");
    } else {
        var pos = Math.floor(Math.random() * preguntas.length);
        randomItem = preguntas[pos];
        document.getElementById("pregunta").innerHTML = randomItem.pregunta;

        document.getElementById("r1").innerText = randomItem.respuesta[0];
        document.getElementById("r2").innerText = randomItem.respuesta[1];
        document.getElementById("r3").innerText = randomItem.respuesta[2];
        document.getElementById("r4").innerText = randomItem.respuesta[3];
        preguntas.splice(pos, 1);
        contador++;

        var valor = document.querySelector('input[name="opciones"]:checked')
        var val = valor.value;


        console.log("XXXXXXX", val)


        if (val == randomItem.correcta) {
            alert("Muy bien");
            score++;

        } else {
            alert("Mal");
        }
    }
    document.getElementById("conysco").style.display = "block";
    console.log(contador)
    document.getElementById("score").innerHTML = "Scor:" + score;
    document.getElementById("contador").innerHTML = "Pregunta:" + contador;


    if (contador === 10) {
        document.getElementById("inicioid").style.display = "none";
        document.getElementById("allpreguntas").style.display = "none";
        document.getElementById("resulta").style.display = "block";
    }
})

//CINE 


var randomItem2 = preguntasCine[Math.floor(Math.random() * preguntasCine.length)];

document.getElementById("pregunta").innerHTML = randomItem2.pregunta2;
document.getElementById("r1").innerText = randomItem2.respuesta2[0];
document.getElementById("r2").innerText = randomItem2.respuesta2[1];
document.getElementById("r3").innerText = randomItem2.respuesta2[2];
document.getElementById("r4").innerText = randomItem2.respuesta2[3];

const btnext2 = document.getElementById("Next");
btnext2.addEventListener("click", function() {
    if (document.querySelector('input[name="opciones"]:checked') === null) {
        alert("Elije una respuesta! Joputi");
    } else {
        var poss = Math.floor(Math.random() * preguntascine.length);
        randomItem2 = preguntascine[poss];



        document.getElementById("pregunta").innerHTML = randomItem2.pregunta2;
        document.getElementById("r1").innerText = randomItem2.respuesta2[0];
        document.getElementById("r2").innerText = randomItem2.respuesta2[1];
        document.getElementById("r3").innerText = randomItem2.respuesta2[2];
        document.getElementById("r4").innerText = randomItem2.respuesta2[3];
        preguntas.splice(poss, 1);
        contador++
        var valor = document.querySelector('input[name="opciones"]:checked')
        var val = valor.value;
        console.log("XXXXXXX", val)


        if (val == randomItem2.correcta2) {
            alert("Muy bien");
            score++;

        } else {
            alert("Mal");
        }
    }
    document.getElementById("score").innerHTML = score;
    document.getElementById("contador").innerHTML = contador;
    if (contador === 10) {
        document.getElementById("inicioid").style.display = "none";
        document.getElementById("allpreguntas").style.display = "none";
        document.getElementById("resulta").style.display = "block";
    }
})




function Reset() {
    document.getElementById("inicioid").style.display = "block";
    document.getElementById("allpreguntas").style.display = "none";
}